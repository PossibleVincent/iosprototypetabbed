//
//  SecondViewController.swift
//  iOSPrototypeTabbed
//
//  Created by Walters,Hunter J on 2/24/19.
//  Copyright © 2019 Walters,Hunter J. All rights reserved.
//
// This code is VERY redundant and prototypey. Will be optimized in the future; consider this a proof of concept
//

import UIKit

class SecondViewController: UIViewController {

    var ledStateArray = ["led00":false, "led01":false, "led02":false, "led03":false, "led04":false, "led10":false, "led11":false, "led12":false, "led13":false, "led14":false, "led20":false, "led21":false, "led22":false, "led23":false, "led24":false, "led30":false, "led31":false, "led32":false, "led33":false, "led34":false, "led40":false, "led41":false, "led42":false, "led43":false, "led44":false]
    
    //Top row (row 0)
    @IBOutlet weak var led00: UIButton!
    @IBOutlet weak var led01: UIButton!
    @IBOutlet weak var led02: UIButton!
    @IBOutlet weak var led03: UIButton!
    @IBOutlet weak var led04: UIButton!
    
    //Upper middle row (row 1)
    @IBOutlet weak var led10: UIButton!
    @IBOutlet weak var led11: UIButton!
    @IBOutlet weak var led12: UIButton!
    @IBOutlet weak var led13: UIButton!
    @IBOutlet weak var led14: UIButton!
    
    //Middle row (row 2)
    @IBOutlet weak var led20: UIButton!
    @IBOutlet weak var led21: UIButton!
    @IBOutlet weak var led22: UIButton!
    @IBOutlet weak var led23: UIButton!
    @IBOutlet weak var led24: UIButton!
    
    //Lower middle row (row 3)
    @IBOutlet weak var led30: UIButton!
    @IBOutlet weak var led31: UIButton!
    @IBOutlet weak var led32: UIButton!
    @IBOutlet weak var led33: UIButton!
    @IBOutlet weak var led34: UIButton!
    
    //Bottom row (row 4)
    @IBOutlet weak var led40: UIButton!
    @IBOutlet weak var led41: UIButton!
    @IBOutlet weak var led42: UIButton!
    @IBOutlet weak var led43: UIButton!
    @IBOutlet weak var led44: UIButton!
    
    //Top row (row 0)
    @IBAction func led00(_ sender: Any) {
        if !(ledStateArray["led00"]!){
            led00.backgroundColor = UIColor.red
            ledStateArray["led00"]! = true
        } else {
            led00.backgroundColor = UIColor.black
            ledStateArray["led00"]! = false
        }
    }
    @IBAction func led01(_ sender: Any) {
        if !(ledStateArray["led01"]!){
            led01.backgroundColor = UIColor.red
            ledStateArray["led01"]! = true
        } else {
            led01.backgroundColor = UIColor.black
            ledStateArray["led01"]! = false
        }
    }
    @IBAction func led02(_ sender: Any) {
        if !(ledStateArray["led02"]!){
            led02.backgroundColor = UIColor.red
            ledStateArray["led02"]! = true
        } else {
            led02.backgroundColor = UIColor.black
            ledStateArray["led02"]! = false
        }
    }
    @IBAction func led03(_ sender: Any) {
        if !(ledStateArray["led03"]!){
            led03.backgroundColor = UIColor.red
            ledStateArray["led03"]! = true
        } else {
            led03.backgroundColor = UIColor.black
            ledStateArray["led03"]! = false
        }
    }
    @IBAction func led04(_ sender: Any) {
        if !(ledStateArray["led04"]!){
            led04.backgroundColor = UIColor.red
            ledStateArray["led04"]! = true
        } else {
            led04.backgroundColor = UIColor.black
            ledStateArray["led04"]! = false
        }
    }
    
    //Upper middle row (row 1)
    @IBAction func led10(_ sender: Any) {
        if !(ledStateArray["led10"]!){
            led10.backgroundColor = UIColor.red
            ledStateArray["led10"]! = true
        } else {
            led10.backgroundColor = UIColor.black
            ledStateArray["led10"]! = false
        }
    }
    @IBAction func led11(_ sender: Any) {
        if !(ledStateArray["led11"]!){
            led11.backgroundColor = UIColor.red
            ledStateArray["led11"]! = true
        } else {
            led11.backgroundColor = UIColor.black
            ledStateArray["led11"]! = false
        }
    }
    @IBAction func led12(_ sender: Any) {
        if !(ledStateArray["led12"]!){
            led12.backgroundColor = UIColor.red
            ledStateArray["led12"]! = true
        } else {
            led12.backgroundColor = UIColor.black
            ledStateArray["led12"]! = false
        }
    }
    @IBAction func led13(_ sender: Any) {
        if !(ledStateArray["led13"]!){
            led13.backgroundColor = UIColor.red
            ledStateArray["led13"]! = true
        } else {
            led13.backgroundColor = UIColor.black
            ledStateArray["led13"]! = false
        }
    }
    @IBAction func led14(_ sender: Any) {
        if !(ledStateArray["led14"]!){
            led14.backgroundColor = UIColor.red
            ledStateArray["led14"]! = true
        } else {
            led14.backgroundColor = UIColor.black
            ledStateArray["led14"]! = false
        }
    }
    
    //Middle row (row 2)
    @IBAction func led20(_ sender: Any) {
        if !(ledStateArray["led20"]!){
            led20.backgroundColor = UIColor.red
            ledStateArray["led20"]! = true
        } else {
            led20.backgroundColor = UIColor.black
            ledStateArray["led20"]! = false
        }
    }
    @IBAction func led21(_ sender: Any) {
        if !(ledStateArray["led21"]!){
            led21.backgroundColor = UIColor.red
            ledStateArray["led21"]! = true
        } else {
            led21.backgroundColor = UIColor.black
            ledStateArray["led21"]! = false
        }
    }
    @IBAction func led22(_ sender: Any) {
        if !(ledStateArray["led22"]!){
            led22.backgroundColor = UIColor.red
            ledStateArray["led22"]! = true
        } else {
            led22.backgroundColor = UIColor.black
            ledStateArray["led22"]! = false
        }
    }
    @IBAction func led23(_ sender: Any) {
        if !(ledStateArray["led23"]!){
            led23.backgroundColor = UIColor.red
            ledStateArray["led23"]! = true
        } else {
            led23.backgroundColor = UIColor.black
            ledStateArray["led23"]! = false
        }
    }
    @IBAction func led24(_ sender: Any) {
        if !(ledStateArray["led24"]!){
            led24.backgroundColor = UIColor.red
            ledStateArray["led24"]! = true
        } else {
            led24.backgroundColor = UIColor.black
            ledStateArray["led24"]! = false
        }
    }
    
    //Lower middle row (row 3)
    @IBAction func led30(_ sender: Any) {
        if !(ledStateArray["led30"]!){
            led30.backgroundColor = UIColor.red
            ledStateArray["led30"]! = true
        } else {
            led30.backgroundColor = UIColor.black
            ledStateArray["led30"]! = false
        }
    }
    @IBAction func led31(_ sender: Any) {
        if !(ledStateArray["led31"]!){
            led31.backgroundColor = UIColor.red
            ledStateArray["led31"]! = true
        } else {
            led31.backgroundColor = UIColor.black
            ledStateArray["led31"]! = false
        }
    }
    @IBAction func led32(_ sender: Any) {
        if !(ledStateArray["led32"]!){
            led32.backgroundColor = UIColor.red
            ledStateArray["led32"]! = true
        } else {
            led32.backgroundColor = UIColor.black
            ledStateArray["led32"]! = false
        }
    }
    @IBAction func led33(_ sender: Any) {
        if !(ledStateArray["led33"]!){
            led33.backgroundColor = UIColor.red
            ledStateArray["led33"]! = true
        } else {
            led33.backgroundColor = UIColor.black
            ledStateArray["led33"]! = false
        }
    }
    @IBAction func led34(_ sender: Any) {
        if !(ledStateArray["led34"]!){
            led34.backgroundColor = UIColor.red
            ledStateArray["led34"]! = true
        } else {
            led34.backgroundColor = UIColor.black
            ledStateArray["led34"]! = false
        }
    }
    
    //Bottom row (row 4)
    @IBAction func led40(_ sender: Any) {
        if !(ledStateArray["led40"]!){
            led40.backgroundColor = UIColor.red
            ledStateArray["led40"]! = true
        } else {
            led40.backgroundColor = UIColor.black
            ledStateArray["led40"]! = false
        }
    }
    @IBAction func led41(_ sender: Any) {
        if !(ledStateArray["led41"]!){
            led41.backgroundColor = UIColor.red
            ledStateArray["led41"]! = true
        } else {
            led41.backgroundColor = UIColor.black
            ledStateArray["led41"]! = false
        }
    }
    @IBAction func led42(_ sender: Any) {
        if !(ledStateArray["led42"]!){
            led42.backgroundColor = UIColor.red
            ledStateArray["led42"]! = true
        } else {
            led42.backgroundColor = UIColor.black
            ledStateArray["led42"]! = false
        }
    }
    @IBAction func led43(_ sender: Any) {
        if !(ledStateArray["led43"]!){
            led43.backgroundColor = UIColor.red
            ledStateArray["led43"]! = true
        } else {
            led43.backgroundColor = UIColor.black
            ledStateArray["led43"]! = false
        }
    }
    @IBAction func led44(_ sender: Any) {
        if !(ledStateArray["led44"]!){
            led44.backgroundColor = UIColor.red
            ledStateArray["led44"]! = true
        } else {
            led44.backgroundColor = UIColor.black
            ledStateArray["led44"]! = false
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
}


